const express = require('express');
const user = express.Router();
const passport = require('passport');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const config = require('../config/database')
const Farm = require('../models/farmdata')
user.post('/register',(req, res, next) => {
    console.log(req.body)
    let newUser = new User({
        name:req.body.name,
        doorNo:req.body.doorNo,
        streetName:req.body.streetName,
        state:req.body.state,
        district:req.body.district,
        taluk:req.body.taluk,
        village:req.body.village,
        pincode:req.body.pincode,
        email:req.body.email,
        fieldNo:req.body.fieldNo,
        fieldSize:req.body.fieldSize,
        username:req.body.username,
        mobile:req.body.mobile,
        password:req.body.password,
        accountType:req.body.accountType
    });
    console.log(req.body)
    User.addUser(newUser, (err,user) =>{
        if (err){
            res.json({success:false,msg:'Failed to register user'});
        } else {
            res.json({success:true,msg:'Registered user Successfully'});
        }
    
    })
});
user.post('/farmDetails',(req,res,next) => {
    let newFarm = new Farm({
        cropName:req.body.cropName,
        cultiArea:req.body.cultiArea,
        fertilizer:req.body.fertilizer,
        insecti:req.body.insecti,
        yeild:req.body.yeild,
        retailer:req.body.retailer,
        season:req.body.season,
        farmerName:req.body.farmerName,
        farmUserName:req.body.farmUserName,
    });
    console.log(req.body.cropName);
    Farm.addFarm(newFarm, (err,farm) => {
        if (err){
            res.json({success:false,msg:'Failed to register farm'});
        } else {
            res.json({success:true,msg:'Farm Added user Successfully'});
        }
    });
});
user.post('/authenticate',(req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;

    User.getUserByUsername(username, (err,user) =>{
        if (err) throw err;
        if (!user) {
            return res.json({success:false,msg:'User not Found'});
        }
        User.comparePassword(password, user.password, (err,isMatch) => {
            if (err) throw err;
            if (isMatch) {
                const token = jwt.sign(user.toJSON(),config.secret,{
                    expiresIn:86400 // 1 day in seconds
                });
                res.json({
                    success:true,
                    token:'JWT '+token,
                    user: {
                        id:user._id,
                        name:user.name,
                        username:user.username,
                        email:user.email
                    }
                });
            } else {
                return res.json({success:false,msg:'Passwords MisMatched'});
            }
        });
    });
    console.log(req.user);

});

user.get('/profile',passport.authenticate('jwt',{session:false}),(req, res, next) => {
    res.json({user:req.user});
});
user.get('/dashboard' ,passport.authenticate('jwt',{session:false}),(req, res, next) => {
    Farm.find({},(err,data) => {
        if(err) throw err;
        else{
            res.json({farmData:data})
        }
    });
});

module.exports = user;