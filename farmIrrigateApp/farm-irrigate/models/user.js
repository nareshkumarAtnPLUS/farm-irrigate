const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

const UserSchema = mongoose.Schema({
    name:{
        type:String,
        require:true
    },
    doorNo:{
        type:String,
        require:true
    },
    streetName:{
        type:String,
        require:true
    },
    state:{
        type:String,
        require:true
    },
    district:{
        type:String,
        require:true
    },
    taluk:{
        type:String,
        require:true
    },
    village:{
        type:String,
        require:true
    },
    pincode:{
        type:String,
        require:true
    },
    email:{
        type:String,
        require:true
    },
    mobile:{
        type:String,
        require:true
    },
    fieldNo:{
        type:String,
        require:true
    },
    fieldSize:{
        type:String,
        require:true
    },
    username:{
        type:String,
        unique:true,
        require:true
    },
    password:{
        type:String,
        require:true
    },
    accountType:{
        type:String,
        require:true
    }
});

const User = module.exports = mongoose.model('User',UserSchema);
module.exports.getUserByUsername = function(username,callback){
    const query = {username:username};
    console.log(query)
    User.findOne(query,callback);
}
module.exports.getUserById = function(id,callback){
    User.findById(id,callback);
}
module.exports.addUser = function(newUser, callback){
    bcrypt.genSalt(10, (err,salt) => {
        bcrypt.hash(newUser.password, salt, (err,hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser.save(callback);
        });
    });
}
module.exports.comparePassword = function(candidatePassword,hash,callback){
    bcrypt.compare(candidatePassword,hash,function(err, isMatch){
        if(err) throw err;
        callback(null,isMatch);
    });
}