const mongoose = require('mongoose');

const FarmSchema = mongoose.Schema({
    cropName:{
        type:String,
        require:true
    },
    cultiArea:{
        type:String,
        require:true
    },
    fertilizer:{
        type:String,
        require:true
    },
    insecti:{
        type:String,
        require:true
    },
    yeild:{
        type:String,
        require:true
    },
    retailer:{
        type:String,
        require:true
    },
    season:{
        type:String,
        require:true
    },
    farmUserName:{
        type:String,
        require:true
    },
    farmerName:{
        type:String,
        require:true
    }
});

const Farm = module.exports = mongoose.model('Farm',FarmSchema);
module.exports.addFarm = function(newFarm, callback){    
    newFarm.save(callback);
}