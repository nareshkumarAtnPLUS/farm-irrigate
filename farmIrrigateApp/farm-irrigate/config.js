const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');
const exphbs = require('express-handlebars');

mongoose.connect(config.database);

mongoose.connection.on('connected', ()=> {
    console.log('Connected to Database '+config.database);
});

mongoose.connection.on('error', (err)=> {
    console.log('Database error '+err);
});


const users = require('./routes/user');

const farmIrrigate = express();

farmIrrigate.use(cors());

// View Engine
farmIrrigate.set('views',path.join(__dirname,'views'));
farmIrrigate.engine('handlebars',exphbs());
farmIrrigate.set('view engine','handlebars');

farmIrrigate.use(express.static(path.join(__dirname,'client')));

farmIrrigate.use(bodyParser.json());
farmIrrigate.use(bodyParser.urlencoded({extended: false}));



//  Passport Initialization
farmIrrigate.use(passport.initialize());
farmIrrigate.use(passport.session());
require('./config/passport')(passport);

farmIrrigate.use('/users',users);

farmIrrigate.get('/',(req,res) => {
    res.send('Invalid Endpoint');
    //res.send('Invalid EndPoint')
    
});
farmIrrigate.get('*',(req,res) => {
    res.sendFile(path.join(__dirname,'client/index.html'));
});


farmIrrigate.set('port', (process.env.PORT || 3000));

module.exports = { farmIrrigate };