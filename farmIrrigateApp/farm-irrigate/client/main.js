(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _components_profile_profile_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/profile/profile.component */ "./src/app/components/profile/profile.component.ts");
/* harmony import */ var _components_reg_form_reg_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/reg-form/reg-form.component */ "./src/app/components/reg-form/reg-form.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_farm_details_farm_details_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/farm-details/farm-details.component */ "./src/app/components/farm-details/farm-details.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _guard_auth_guard__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./guard/auth.guard */ "./src/app/guard/auth.guard.ts");
/* harmony import */ var _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/dashboard/dashboard.component */ "./src/app/components/dashboard/dashboard.component.ts");










var routes = [
    { path: '', component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] },
    { path: 'register', component: _components_reg_form_reg_form_component__WEBPACK_IMPORTED_MODULE_2__["RegFormComponent"] },
    { path: 'login', component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_7__["LoginComponent"] },
    { path: 'dashboard', component: _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_9__["DashboardComponent"] },
    { path: 'farmDetails', component: _components_farm_details_farm_details_component__WEBPACK_IMPORTED_MODULE_4__["FarmDetailsComponent"], canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_8__["AuthGuard"]] },
    { path: 'profile', component: _components_profile_profile_component__WEBPACK_IMPORTED_MODULE_1__["ProfileComponent"], canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_8__["AuthGuard"]] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\r\n<app-main-nav>\r\n<main>\r\n    <ng-flash-message></ng-flash-message>\r\n    <router-outlet></router-outlet>\r\n    \r\n</main>\r\n</app-main-nav>\r\n\r\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "main {\n  margin-top: 5rem;\n  width: 80%;\n  margin: auto; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvRDpcXFNvdXJjZV9Db2RlXFxIYWNLYVRob25cXGZhcm0taXJyaWdhdGVcXGZhcm1JcnJpZ2F0ZUFwcFxcYW5ndWxhci1zcmMvc3JjXFxhcHBcXGFwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGlCQUFnQjtFQUNoQixXQUFVO0VBQ1YsYUFBWSxFQUNiIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsibWFpbiB7XHJcbiAgbWFyZ2luLXRvcDogNXJlbTtcclxuICB3aWR0aDogODAlO1xyXG4gIG1hcmdpbjogYXV0bztcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'angular-src';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _theme_material_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./theme/material.module */ "./src/app/theme/material.module.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _components_main_nav_main_nav_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/main-nav/main-nav.component */ "./src/app/components/main-nav/main-nav.component.ts");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _components_reg_form_reg_form_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/reg-form/reg-form.component */ "./src/app/components/reg-form/reg-form.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_farm_details_farm_details_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/farm-details/farm-details.component */ "./src/app/components/farm-details/farm-details.component.ts");
/* harmony import */ var _components_profile_profile_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/profile/profile.component */ "./src/app/components/profile/profile.component.ts");
/* harmony import */ var _services_validate_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./services/validate.service */ "./src/app/services/validate.service.ts");
/* harmony import */ var ng_flash_messages__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ng-flash-messages */ "./node_modules/ng-flash-messages/ng-flash-messages.umd.js");
/* harmony import */ var ng_flash_messages__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(ng_flash_messages__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _services_form_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./services/form.service */ "./src/app/services/form.service.ts");
/* harmony import */ var _guard_auth_guard__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./guard/auth.guard */ "./src/app/guard/auth.guard.ts");
/* harmony import */ var _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./components/dashboard/dashboard.component */ "./src/app/components/dashboard/dashboard.component.ts");






















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
                _components_main_nav_main_nav_component__WEBPACK_IMPORTED_MODULE_8__["MainNavComponent"],
                _components_reg_form_reg_form_component__WEBPACK_IMPORTED_MODULE_10__["RegFormComponent"],
                _components_login_login_component__WEBPACK_IMPORTED_MODULE_12__["LoginComponent"],
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_13__["HomeComponent"],
                _components_farm_details_farm_details_component__WEBPACK_IMPORTED_MODULE_14__["FarmDetailsComponent"],
                _components_profile_profile_component__WEBPACK_IMPORTED_MODULE_15__["ProfileComponent"],
                _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_21__["DashboardComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__["BrowserAnimationsModule"],
                _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_9__["LayoutModule"],
                _theme_material_module__WEBPACK_IMPORTED_MODULE_2__["MaterialModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_11__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_18__["HttpClientModule"],
                ng_flash_messages__WEBPACK_IMPORTED_MODULE_17__["NgFlashMessagesModule"].forRoot()
            ],
            providers: [_services_validate_service__WEBPACK_IMPORTED_MODULE_16__["ValidateService"],
                _services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"], _services_form_service__WEBPACK_IMPORTED_MODULE_19__["FormService"],
                _guard_auth_guard__WEBPACK_IMPORTED_MODULE_20__["AuthGuard"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/dashboard/dashboard.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2 class=\"page-header\">Farmer Details</h2>\r\n  <mat-card *ngFor=\"let data of datum\" class=\"return-card\">\r\n      <mat-card-header>\r\n        <mat-card-title>Farmer Name: {{data.farmerName}}</mat-card-title>\r\n      </mat-card-header>\r\n      <mat-card-content>\r\n          <mat-accordion>\r\n          <mat-expansion-panel (opened)=\"panelOpenState = true\"\r\n          (closed)=\"panelOpenState = false\">\r\n          <mat-expansion-panel-header>\r\n          <mat-panel-title>\r\n          Crop Name\r\n          </mat-panel-title>\r\n          <mat-panel-description>\r\n              {{data.cropName}}\r\n          </mat-panel-description>\r\n          </mat-expansion-panel-header>\r\n          <p>Fertilizer Used : {{data.fertilizer}}</p>\r\n          <p>Insecticide Used : {{data.insecticide}}</p>\r\n          <p>Cultivated Area : {{data.cultiArea}}</p>\r\n          <p>Cultivated Season : {{data.season}}</p>\r\n          <p>Yeild : {{data.yeild}}</p>\r\n          <p>Sold To Retailer : {{data.retailer}}</p>\r\n          <p>Farmer User Name : {{data.farmUserName}}</p>\r\n          </mat-expansion-panel>\r\n          </mat-accordion>\r\n      </mat-card-content>\r\n    \r\n"

/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/components/dashboard/dashboard.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/dashboard/dashboard.component.ts ***!
  \*************************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ng_flash_messages__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng-flash-messages */ "./node_modules/ng-flash-messages/ng-flash-messages.umd.js");
/* harmony import */ var ng_flash_messages__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(ng_flash_messages__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");






var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(authService, router, flashMessage, http) {
        this.authService = authService;
        this.router = router;
        this.flashMessage = flashMessage;
        this.http = http;
    }
    DashboardComponent.prototype.getData = function () {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': this.authService.authToken
            })
        };
        this.loadToken();
        console.log(this.authService.authToken);
        return this.http.get('http://localhost:3000/users/dashboard', httpOptions);
    };
    DashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getData().subscribe(function (profile) {
            _this.datum = profile.farmData;
            if (!(_this.datum == null)) {
                console.log(_this.datum);
            }
            else {
                console.log('UnAuthorized Access!');
            }
        }, function (err) {
            return false;
        });
    };
    DashboardComponent.prototype.loadToken = function () {
        var token = localStorage.getItem('id_token');
        this.authService.authToken = token;
    };
    DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/components/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/components/dashboard/dashboard.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            ng_flash_messages__WEBPACK_IMPORTED_MODULE_4__["NgFlashMessageService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/components/farm-details/farm-details.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/farm-details/farm-details.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\r\n  <h2 class=\"page-header\">Farming Details</h2>\r\n  <mat-card layout-align=\"center center\" class=\"form-card\">\r\n      <mat-card-header>\r\n          <mat-card-title>Crop Yeild Form</mat-card-title>\r\n      </mat-card-header>\r\n\r\n  <form (submit)=\"onFarmSubmit()\">\r\n      <mat-card-content>\r\n      <div class=\"row\">\r\n          <mat-form-field class=\"reg-full-width\">\r\n              <input matInput type=\"text\" [(ngModel)]=\"cropName\" name=\"cropName\" class=\"form-control\" id=\"cropName\" placeholder=\"Enter Crop Name\">\r\n          </mat-form-field>\r\n      </div>\r\n      <div class=\"row\">\r\n          <mat-form-field class=\"reg-full-width\">\r\n              <input matInput type=\"text\" [(ngModel)]=\"cultiArea\" name=\"cultiArea\" class=\"form-control\" id=\"cultiArea\" placeholder=\"Cultivated Area (in sq.ft)\">\r\n          </mat-form-field>\r\n      </div>\r\n      <div class=\"row\">\r\n          <mat-form-field class=\"reg-full-width\">\r\n              <input matInput type=\"text\" [(ngModel)]=\"fertilizer\" name=\"fertilizer\" class=\"form-control\" id=\"fertilizer\" placeholder=\"Fertilizer Used\">\r\n          </mat-form-field>\r\n      </div>\r\n      <div class=\"row\">\r\n          <mat-form-field class=\"reg-full-width\">\r\n              <input matInput type=\"text\" [(ngModel)]=\"insecti\" name=\"insecti\" class=\"form-control\" id=\"insecti\" placeholder=\"insecticide used\">\r\n          </mat-form-field>\r\n      </div>\r\n      <div class=\"row\">\r\n          <mat-form-field class=\"reg-full-width\">\r\n              <input matInput type=\"text\" [(ngModel)]=\"yeild\" name=\"yeild\" class=\"form-control\" id=\"yeild\" placeholder=\"Yeild Amount in Kg.\">\r\n          </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"row\">\r\n          <mat-form-field class=\"reg-full-width\">\r\n              <input matInput type=\"text\" [(ngModel)]=\"retailer\" name=\"retailer\" class=\"form-control\" id=\"retailer\" placeholder=\"Sold To - Retailer Name\">\r\n          </mat-form-field>\r\n      </div>\r\n      <div class=\"row\">\r\n          <mat-form-field class=\"reg-full-width\">\r\n              <input matInput type=\"text\" [(ngModel)]=\"soldAmount\" name=\"soldAmount\" class=\"form-control\" id=\"soldAmount\" placeholder=\"Sold Amount\">\r\n          </mat-form-field>\r\n      </div>\r\n      <div class=\"row\">\r\n        <mat-form-field class=\"reg-full-width\">\r\n            <input matInput type=\"text\" [(ngModel)]=\"farmerName\" name=\"farmerName\" class=\"form-control\" id=\"farmerName\" placeholder=\"Farmer Name\">\r\n        </mat-form-field>\r\n    </div>\r\n      <div class=\"row\">\r\n          <mat-form-field class=\"reg-full-width\">\r\n              <input matInput type=\"text\" [(ngModel)]=\"farmUserName\" name=\"farmUserName\" class=\"form-control\" id=\"farmUserName\" placeholder=\"Farmer UserName\">\r\n          </mat-form-field>\r\n      </div>\r\n      <div class=\"row\">\r\n        <mat-form-field class=\"reg-full-width\">\r\n            <input matInput type=\"text\" [(ngModel)]=\"season\" name=\"season\" class=\"form-control\" id=\"season\" placeholder=\"Season\">\r\n        </mat-form-field>\r\n    </div>\r\n    </mat-card-content>\r\n    <mat-card-actions>\r\n        <button mat-raised-button color=\"accent\" type=\"submit\">Register</button>\r\n    </mat-card-actions>\r\n  </form>\r\n  </mat-card>\r\n  </div>\r\n"

/***/ }),

/***/ "./src/app/components/farm-details/farm-details.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/components/farm-details/farm-details.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".full-width {\n  width: 100%; }\n\n.shipping-card {\n  min-width: 120px;\n  margin: 20px auto; }\n\n.mat-radio-button {\n  display: block;\n  margin: 5px 0; }\n\n.row {\n  display: flex;\n  flex-direction: row; }\n\n.col {\n  flex: 1;\n  margin-right: 20px; }\n\n.col:last-child {\n  margin-right: 0; }\n\n.reg-full-width {\n  width: 100%; }\n\n.form-card {\n  min-width: 150px;\n  max-width: 700px;\n  width: 100%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9mYXJtLWRldGFpbHMvRDpcXFNvdXJjZV9Db2RlXFxIYWNLYVRob25cXGZhcm0taXJyaWdhdGVcXGZhcm1JcnJpZ2F0ZUFwcFxcYW5ndWxhci1zcmMvc3JjXFxhcHBcXGNvbXBvbmVudHNcXGZhcm0tZGV0YWlsc1xcZmFybS1kZXRhaWxzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBVyxFQUNaOztBQUVEO0VBQ0UsaUJBQWdCO0VBQ2hCLGtCQUFpQixFQUNsQjs7QUFFRDtFQUNFLGVBQWM7RUFDZCxjQUFhLEVBQ2Q7O0FBRUQ7RUFDRSxjQUFhO0VBQ2Isb0JBQW1CLEVBQ3BCOztBQUVEO0VBQ0UsUUFBTztFQUNQLG1CQUFrQixFQUNuQjs7QUFFRDtFQUNFLGdCQUFlLEVBQ2hCOztBQUVEO0VBQ0UsWUFBVyxFQUNaOztBQUNEO0VBQ0UsaUJBQWdCO0VBQ2hCLGlCQUFnQjtFQUNoQixZQUFXLEVBQ1oiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2Zhcm0tZGV0YWlscy9mYXJtLWRldGFpbHMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZnVsbC13aWR0aCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbiAgXHJcbiAgLnNoaXBwaW5nLWNhcmQge1xyXG4gICAgbWluLXdpZHRoOiAxMjBweDtcclxuICAgIG1hcmdpbjogMjBweCBhdXRvO1xyXG4gIH1cclxuICBcclxuICAubWF0LXJhZGlvLWJ1dHRvbiB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIG1hcmdpbjogNXB4IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5yb3cge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgfVxyXG4gIFxyXG4gIC5jb2wge1xyXG4gICAgZmxleDogMTtcclxuICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxuICB9XHJcbiAgXHJcbiAgLmNvbDpsYXN0LWNoaWxkIHtcclxuICAgIG1hcmdpbi1yaWdodDogMDtcclxuICB9XHJcbiAgXHJcbiAgLnJlZy1mdWxsLXdpZHRoIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuICAuZm9ybS1jYXJkIHtcclxuICAgIG1pbi13aWR0aDogMTUwcHg7XHJcbiAgICBtYXgtd2lkdGg6IDcwMHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4gICJdfQ== */"

/***/ }),

/***/ "./src/app/components/farm-details/farm-details.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/farm-details/farm-details.component.ts ***!
  \*******************************************************************/
/*! exports provided: FarmDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FarmDetailsComponent", function() { return FarmDetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _services_validate_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/validate.service */ "./src/app/services/validate.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng_flash_messages__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng-flash-messages */ "./node_modules/ng-flash-messages/ng-flash-messages.umd.js");
/* harmony import */ var ng_flash_messages__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(ng_flash_messages__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var FarmDetailsComponent = /** @class */ (function () {
    function FarmDetailsComponent(validateService, flashMessage, router, http) {
        this.validateService = validateService;
        this.flashMessage = flashMessage;
        this.router = router;
        this.http = http;
    }
    FarmDetailsComponent.prototype.onFarmSubmit = function () {
        var _this = this;
        var farmData = {
            cropName: this.cropName,
            cultiArea: this.cultiArea,
            fertilizer: this.fertilizer,
            insecti: this.insecti,
            yeild: this.yeild,
            retailer: this.retailer,
            season: this.season,
            farmUserName: this.farmUserName,
            farmerName: this.farmerName,
        };
        console.log(farmData);
        if (!this.validateService.validateFarm(farmData)) {
            this.flashMessage.showFlashMessage({
                messages: ['Please Fill all fields'],
                dismissible: true, timeout: 3000, type: 'danger'
            });
            return false;
        }
        var req = this.http.post('http://localhost:3000/users/farmDetails', farmData).subscribe(function (res) {
            console.log(res);
            _this.flashMessage.showFlashMessage({
                messages: ['Farm Data Successfully Uploaded to Portal'],
                dismissible: true, timeout: 3000, type: 'success'
            });
            _this.router.navigate(['/farmDetails']);
        }, function (err) {
            console.log('Error Occured');
            _this.flashMessage.showFlashMessage({
                messages: ['Something Went Wrong'],
                dismissible: true, timeout: 3000, type: 'danger'
            });
            _this.router.navigate(['/']);
        });
        console.log(req);
    };
    FarmDetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-farm-details',
            template: __webpack_require__(/*! ./farm-details.component.html */ "./src/app/components/farm-details/farm-details.component.html"),
            styles: [__webpack_require__(/*! ./farm-details.component.scss */ "./src/app/components/farm-details/farm-details.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_validate_service__WEBPACK_IMPORTED_MODULE_2__["ValidateService"],
            ng_flash_messages__WEBPACK_IMPORTED_MODULE_4__["NgFlashMessageService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], FarmDetailsComponent);
    return FarmDetailsComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"block\">\r\n  <h1>Home</h1>\r\n    <mat-card>\r\n        <mat-card-header>\r\n            <h1 class=\"display-1\" ngClass.xs=\"display-3 text-center\" ngClass.sm=\"display-3\">Farm Portal</h1>\r\n        </mat-card-header>\r\n        <img mat-card-image src=\"assets/farm-home.jpg\" alt=\"Photo of a Farmer\">\r\n        <mat-card-content>\r\n            <p>Agricultre is our wisest pursuit,because it will inthe end contibute most to real wealth,good morals,unhapinees.</p>\r\n            <p>Give a Try to our Farm API !</p>\r\n        </mat-card-content>\r\n        <mat-action-row>\r\n            <button mat-raised-button color=\"accent\" routerLink=\"/register\">Register</button>&nbsp;\r\n            <button mat-raised-button color=\"accent\" routerLink=\"/login\">Login</button>\r\n        </mat-action-row>\r\n    </mat-card>\r\n    \r\n    "

/***/ }),

/***/ "./src/app/components/home/home.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/components/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/components/home/home.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/login/login.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\r\n    <h2 class=\"page-header\">Login</h2>\r\n    <mat-card layout-align=\"center center\" class=\"form-card\">\r\n        <mat-card-header>\r\n            <mat-card-title>Login Form</mat-card-title>\r\n        </mat-card-header>\r\n\r\n    <form (submit)=\"onLoginSubmit()\">\r\n        <mat-card-content>\r\n        <div class=\"row\">\r\n            <mat-form-field class=\"reg-full-width\">\r\n                <input matInput type=\"text\" [(ngModel)]=\"username\" name=\"username\" class=\"form-control\" id=\"username\" placeholder=\"Enter UserName\">\r\n            </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"row\">\r\n            <mat-form-field class=\"reg-full-width\">\r\n                <input matInput type=\"password\" [(ngModel)]=\"password\" name=\"password\" class=\"form-control\" id=\"password\" placeholder=\"Enter Password\">\r\n            </mat-form-field>\r\n        </div>\r\n      </mat-card-content>\r\n      <mat-card-actions>\r\n          <button mat-raised-button color=\"accent\" type=\"submit\">Login</button>\r\n      </mat-card-actions>\r\n    </form>\r\n\r\n    </mat-card>\r\n    </div>\r\n"

/***/ }),

/***/ "./src/app/components/login/login.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".full-width {\n  width: 100%; }\n\n.shipping-card {\n  min-width: 120px;\n  margin: 20px auto; }\n\n.mat-radio-button {\n  display: block;\n  margin: 5px 0; }\n\n.row {\n  display: flex;\n  flex-direction: row; }\n\n.col {\n  flex: 1;\n  margin-right: 20px; }\n\n.col:last-child {\n  margin-right: 0; }\n\n.reg-full-width {\n  width: 100%; }\n\n.form-card {\n  min-width: 150px;\n  max-width: 700px;\n  width: 100%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9sb2dpbi9EOlxcU291cmNlX0NvZGVcXEhhY0thVGhvblxcZmFybS1pcnJpZ2F0ZVxcZmFybUlycmlnYXRlQXBwXFxhbmd1bGFyLXNyYy9zcmNcXGFwcFxcY29tcG9uZW50c1xcbG9naW5cXGxvZ2luLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBVyxFQUNaOztBQUVEO0VBQ0UsaUJBQWdCO0VBQ2hCLGtCQUFpQixFQUNsQjs7QUFFRDtFQUNFLGVBQWM7RUFDZCxjQUFhLEVBQ2Q7O0FBRUQ7RUFDRSxjQUFhO0VBQ2Isb0JBQW1CLEVBQ3BCOztBQUVEO0VBQ0UsUUFBTztFQUNQLG1CQUFrQixFQUNuQjs7QUFFRDtFQUNFLGdCQUFlLEVBQ2hCOztBQUVEO0VBQ0UsWUFBVyxFQUNaOztBQUNEO0VBQ0UsaUJBQWdCO0VBQ2hCLGlCQUFnQjtFQUNoQixZQUFXLEVBQ1oiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZ1bGwtd2lkdGgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uc2hpcHBpbmctY2FyZCB7XHJcbiAgbWluLXdpZHRoOiAxMjBweDtcclxuICBtYXJnaW46IDIwcHggYXV0bztcclxufVxyXG5cclxuLm1hdC1yYWRpby1idXR0b24ge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbjogNXB4IDA7XHJcbn1cclxuXHJcbi5yb3cge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxufVxyXG5cclxuLmNvbCB7XHJcbiAgZmxleDogMTtcclxuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XHJcbn1cclxuXHJcbi5jb2w6bGFzdC1jaGlsZCB7XHJcbiAgbWFyZ2luLXJpZ2h0OiAwO1xyXG59XHJcblxyXG4ucmVnLWZ1bGwtd2lkdGgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcbi5mb3JtLWNhcmQge1xyXG4gIG1pbi13aWR0aDogMTUwcHg7XHJcbiAgbWF4LXdpZHRoOiA3MDBweDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var ng_flash_messages__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng-flash-messages */ "./node_modules/ng-flash-messages/ng-flash-messages.umd.js");
/* harmony import */ var ng_flash_messages__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ng_flash_messages__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_validate_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../services/validate.service */ "./src/app/services/validate.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var LoginComponent = /** @class */ (function () {
    function LoginComponent(router, flashMessage, http, validateService, authService) {
        this.router = router;
        this.flashMessage = flashMessage;
        this.http = http;
        this.validateService = validateService;
        this.authService = authService;
    }
    LoginComponent.prototype.onLoginSubmit = function () {
        var _this = this;
        var user = {
            username: this.username,
            password: this.password
        };
        if (!this.validateService.validateLogin(user)) {
            this.flashMessage.showFlashMessage({
                messages: ['Please Fill all fields'],
                dismissible: true, timeout: 3000, type: 'danger'
            });
            return false;
        }
        var req = this.http.post('http://localhost:3000/users/authenticate', user).subscribe(function (res) {
            console.log(res.success);
            if (res.success) {
                _this.authService.storeUserData(res.token, res.user);
                _this.flashMessage.showFlashMessage({
                    messages: ['You Are Now Logged In'],
                    dismissible: true, timeout: 3000, type: 'success'
                });
                _this.router.navigate(['/profile']);
                console.log(res.token, res.user);
            }
            else {
                _this.flashMessage.showFlashMessage({
                    messages: ['Check Login Credentials,User Name or password mismatch'],
                    dismissible: true, timeout: 5000, type: 'danger'
                });
                _this.router.navigate(['/login']);
            }
        }, function (err) {
            console.log('Error Occured');
            _this.flashMessage.showFlashMessage({
                messages: ['Something Went Wrong'],
                dismissible: true, timeout: 3000, type: 'danger'
            });
            _this.router.navigate(['/login']);
        });
        console.log(req);
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/components/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/components/login/login.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            ng_flash_messages__WEBPACK_IMPORTED_MODULE_2__["NgFlashMessageService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"],
            _services_validate_service__WEBPACK_IMPORTED_MODULE_3__["ValidateService"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/main-nav/main-nav.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/main-nav/main-nav.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container class=\"sidenav-container\">\r\n  <mat-sidenav #drawer class=\"sidenav\" fixedInViewport=\"true\"\r\n      [attr.role]=\"(isHandset$ | async) ? 'dialog' : 'navigation'\"\r\n      [mode]=\"(isHandset$ | async) ? 'over' : 'side'\"\r\n      [opened]=\"false\">\r\n    <mat-toolbar>Menu</mat-toolbar>\r\n    <mat-nav-list>\r\n      <a mat-list-item routerLink=\"/\">Home</a>\r\n      <a mat-list-item *ngIf=\"authService.isLoggedIn()\" routerLink=\"/profile\">Profile</a>\r\n      <a mat-list-item *ngIf=\"authService.isLoggedIn()\" routerLink=\"/dashboard\">Dashboard</a>\r\n      <a mat-list-item *ngIf=\"authService.isLoggedIn()\" routerLink=\"/farmDetails\">Farm Details</a>\r\n      <mat-accordion>\r\n        <mat-expansion-panel>\r\n            <mat-expansion-panel-header>\r\n                <mat-panel-title>\r\n                  Account\r\n                </mat-panel-title>\r\n              </mat-expansion-panel-header>\r\n              <a mat-list-item *ngIf=\"!authService.isLoggedIn()\" routerLink=\"/register\">Register</a>\r\n              <a mat-list-item *ngIf=\"!authService.isLoggedIn()\" routerLink=\"/login\">Login</a>\r\n              <a mat-list-item *ngIf=\"authService.isLoggedIn()\" (click)=\"onLogoutClick()\">Logout</a>\r\n            </mat-expansion-panel>\r\n          </mat-accordion>\r\n    </mat-nav-list>\r\n  </mat-sidenav>\r\n  <mat-sidenav-content>\r\n    <mat-toolbar color=\"primary\">\r\n      <button\r\n        type=\"button\"\r\n        aria-label=\"Toggle sidenav\"\r\n        mat-icon-button\r\n        (click)=\"drawer.toggle()\"\r\n        *ngIf=\"isHandset$ | async\">\r\n        <mat-icon aria-label=\"Side nav toggle icon\">menu</mat-icon>\r\n      </button>\r\n      <span>FarmIrrigate API</span>\r\n      <span class=\"spacer\"></span>\r\n      <div class=\"navbar\" *ngIf=\"!(isHandset$ | async)\">\r\n        <button mat-button (click)=\"onClick\" [routerLinkActive]=\"['active']\" routerLink=\"/\">Home</button>\r\n        <button mat-button *ngIf=\"authService.isLoggedIn()\" (click)=\"onClick\" [routerLinkActive]=\"['active']\" routerLink=\"/profile\">Profile</button>\r\n        <button mat-button *ngIf=\"authService.isLoggedIn()\" (click)=\"onClick\" [routerLinkActive]=\"['active']\" routerLink=\"/farmDetails\">Farm Details</button>\r\n        <button mat-button *ngIf=\"authService.isLoggedIn()\" (click)=\"onClick\" [routerLinkActive]=\"['active']\" routerLink=\"/dashboard\">Dashboard</button>\r\n        <button mat-button (click)=\"onClick\" [routerLinkActive]=\"['active']\" [matMenuTriggerFor]=\"menu\">Account</button>\r\n\r\n        <mat-menu #menu=\"matMenu\">\r\n            <button *ngIf=\"!authService.isLoggedIn()\" routerLink=\"/register\" mat-menu-item>Register</button>\r\n            <button *ngIf=\"!authService.isLoggedIn()\" routerLink=\"/login\" mat-menu-item>Login</button>\r\n            <button *ngIf=\"authService.isLoggedIn()\" routerLink=\"/profile\" mat-menu-item>Profile</button>\r\n            <button *ngIf=\"authService.isLoggedIn()\" href=\"#\" (click)=\"onLogoutClick()\" mat-menu-item>Logout</button>\r\n          </mat-menu>\r\n      </div>\r\n    </mat-toolbar>\r\n    <!-- Add Content Here -->\r\n    <ng-content></ng-content>\r\n  </mat-sidenav-content>\r\n</mat-sidenav-container>\r\n"

/***/ }),

/***/ "./src/app/components/main-nav/main-nav.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/components/main-nav/main-nav.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sidenav-container {\n  height: 100%; }\n\n.sidenav {\n  width: 200px;\n  z-index: 5; }\n\n.sidenav .mat-toolbar {\n  background: inherit; }\n\n.mat-toolbar.mat-primary {\n  position: -webkit-sticky;\n  position: sticky;\n  top: 0;\n  z-index: 5; }\n\n.spacer {\n  flex: 1 1 auto; }\n\n.mat-toolbar a {\n  display: inline-block;\n  margin: 0 10px;\n  color: white;\n  text-decoration: none; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tYWluLW5hdi9EOlxcU291cmNlX0NvZGVcXEhhY0thVGhvblxcZmFybS1pcnJpZ2F0ZVxcZmFybUlycmlnYXRlQXBwXFxhbmd1bGFyLXNyYy9zcmNcXGFwcFxcY29tcG9uZW50c1xcbWFpbi1uYXZcXG1haW4tbmF2LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBWSxFQUNiOztBQUVEO0VBQ0UsYUFBWTtFQUNaLFdBQVUsRUFDWDs7QUFFRDtFQUNFLG9CQUFtQixFQUNwQjs7QUFFRDtFQUNFLHlCQUFnQjtFQUFoQixpQkFBZ0I7RUFDaEIsT0FBTTtFQUNOLFdBQVUsRUFDWDs7QUFDRDtFQUNFLGVBQWMsRUFDZjs7QUFFRDtFQUNFLHNCQUFxQjtFQUNyQixlQUFjO0VBQ2QsYUFBWTtFQUNaLHNCQUFxQixFQUN0QiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbWFpbi1uYXYvbWFpbi1uYXYuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2lkZW5hdi1jb250YWluZXIge1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG5cclxuLnNpZGVuYXYge1xyXG4gIHdpZHRoOiAyMDBweDtcclxuICB6LWluZGV4OiA1O1xyXG59XHJcblxyXG4uc2lkZW5hdiAubWF0LXRvb2xiYXIge1xyXG4gIGJhY2tncm91bmQ6IGluaGVyaXQ7XHJcbn1cclxuXHJcbi5tYXQtdG9vbGJhci5tYXQtcHJpbWFyeSB7XHJcbiAgcG9zaXRpb246IHN0aWNreTtcclxuICB0b3A6IDA7XHJcbiAgei1pbmRleDogNTtcclxufVxyXG4uc3BhY2VyIHtcclxuICBmbGV4OiAxIDEgYXV0bztcclxufVxyXG5cclxuLm1hdC10b29sYmFyIGF7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIG1hcmdpbjogMCAxMHB4O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbn1cclxuXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/main-nav/main-nav.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/main-nav/main-nav.component.ts ***!
  \***********************************************************/
/*! exports provided: MainNavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainNavComponent", function() { return MainNavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var ng_flash_messages__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng-flash-messages */ "./node_modules/ng-flash-messages/ng-flash-messages.umd.js");
/* harmony import */ var ng_flash_messages__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(ng_flash_messages__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var src_app_services_validate_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/validate.service */ "./src/app/services/validate.service.ts");









var MainNavComponent = /** @class */ (function () {
    function MainNavComponent(breakpointObserver, router, flashMessage, http, validateService, authService) {
        this.breakpointObserver = breakpointObserver;
        this.router = router;
        this.flashMessage = flashMessage;
        this.http = http;
        this.validateService = validateService;
        this.authService = authService;
        //opener:false;
        this.isHandset$ = this.breakpointObserver.observe(_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_5__["Breakpoints"].Handset)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (result) { return result.matches; }));
    }
    MainNavComponent.prototype.onLogoutClick = function () {
        this.authService.logout();
        this.flashMessage.showFlashMessage({
            messages: ['You Are Now Logged Out'],
            dismissible: true, timeout: 3000, type: 'success'
        });
        this.router.navigate(['/login']);
        return false;
    };
    MainNavComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
            selector: 'app-main-nav',
            template: __webpack_require__(/*! ./main-nav.component.html */ "./src/app/components/main-nav/main-nav.component.html"),
            styles: [__webpack_require__(/*! ./main-nav.component.scss */ "./src/app/components/main-nav/main-nav.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_5__["BreakpointObserver"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            ng_flash_messages__WEBPACK_IMPORTED_MODULE_7__["NgFlashMessageService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            src_app_services_validate_service__WEBPACK_IMPORTED_MODULE_8__["ValidateService"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], MainNavComponent);
    return MainNavComponent;
}());



/***/ }),

/***/ "./src/app/components/profile/profile.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/profile/profile.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>Profile</h1>\r\n  <mat-card *ngIf=\"user\" class=\"example-card\">\r\n      <mat-card-header>\r\n          <h2 *ngIf=\"accountType\">Admin</h2>\r\n        <mat-card-title>{{user.accountType}} {{user.name}}</mat-card-title>\r\n      </mat-card-header>\r\n      <mat-card-content>\r\n          <mat-accordion>\r\n          <mat-expansion-panel (opened)=\"panelOpenState = true\"\r\n          (closed)=\"panelOpenState = false\">\r\n          <mat-expansion-panel-header>\r\n          <mat-panel-title>\r\n          User Name\r\n          </mat-panel-title>\r\n          <mat-panel-description>\r\n              {{user.username}}\r\n          </mat-panel-description>\r\n          </mat-expansion-panel-header>\r\n          <p>Door No {{ user.doorNo }}</p>\r\n          <p>Street Name {{user.streetName}}</p>\r\n          <p>Email  {{user.email}}</p>\r\n          <p>User Name  {{user.userName}}</p>\r\n          <p>Mobile Number  {{user.mobileNumber}}</p>\r\n          <p>District  {{user.district}}</p>\r\n          <p>Village  {{user.village}}</p>\r\n          <p>Pin Code  {{user.pincode}}</p>\r\n          </mat-expansion-panel>\r\n          </mat-accordion>\r\n      </mat-card-content>\r\n"

/***/ }),

/***/ "./src/app/components/profile/profile.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/components/profile/profile.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcHJvZmlsZS9wcm9maWxlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/profile/profile.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/profile/profile.component.ts ***!
  \*********************************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng_flash_messages__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng-flash-messages */ "./node_modules/ng-flash-messages/ng-flash-messages.umd.js");
/* harmony import */ var ng_flash_messages__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ng_flash_messages__WEBPACK_IMPORTED_MODULE_5__);






var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(authService, router, flashMessage, http) {
        this.authService = authService;
        this.router = router;
        this.flashMessage = flashMessage;
        this.http = http;
    }
    ProfileComponent.prototype.getProfile = function () {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': this.authService.authToken
            })
        };
        this.loadToken();
        console.log('user Data', this.authService.authToken);
        return this.http.get('http://localhost:3000/users/profile', httpOptions);
    };
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getProfile().subscribe(function (profile) {
            _this.user = profile.user;
            if (!(_this.user == null)) {
            }
            else {
                console.log('UnAuthorized Access!');
            }
        }, function (err) {
            return false;
        });
    };
    ProfileComponent.prototype.loadToken = function () {
        var token = localStorage.getItem('id_token');
        this.authService.authToken = token;
    };
    ProfileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__(/*! ./profile.component.html */ "./src/app/components/profile/profile.component.html"),
            styles: [__webpack_require__(/*! ./profile.component.scss */ "./src/app/components/profile/profile.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            ng_flash_messages__WEBPACK_IMPORTED_MODULE_5__["NgFlashMessageService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/components/reg-form/reg-form.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/reg-form/reg-form.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\r\n    <h2 class=\"page-header\">Register</h2>\r\n    <mat-card layout-align=\"center center\" class=\"form-card\">\r\n        <mat-card-header>\r\n            <mat-card-title>\r\n                Registration Form \r\n                <span class=\"column\">\r\n                    <mat-radio-group name=\"accountType\" [(ngModel)]=\"accountType\">\r\n                        <mat-radio-button value=\"Farmer\">Farmer</mat-radio-button>\r\n                        <mat-radio-button value=\"Admin\">Irrigator</mat-radio-button>\r\n                    </mat-radio-group>\r\n                </span>\r\n            </mat-card-title>\r\n        </mat-card-header>\r\n\r\n    <form (submit)=\"onRegisterSubmit()\">\r\n        <mat-card-content>\r\n        <div class=\"row\">\r\n            <mat-form-field class=\"reg-full-width\">\r\n                <input matInput type=\"text\" [(ngModel)]=\"name\" name=\"name\" class=\"form-control\" id=\"name\" placeholder=\"Enter Name\">\r\n            </mat-form-field>\r\n        </div>\r\n        <div class=\"row\">\r\n            <mat-form-field class=\"reg-full-width\">\r\n                <input matInput type=\"text\" [(ngModel)]=\"doorNo\" name=\"doorNo\" class=\"form-control\" id=\"doorNo\" placeholder=\"Door No\">\r\n            </mat-form-field>\r\n        </div>\r\n        <div class=\"row\">\r\n            <mat-form-field class=\"reg-full-width\">\r\n                <input matInput type=\"text\" [(ngModel)]=\"streetName\" name=\"streetName\" class=\"form-control\" id=\"streetName\" placeholder=\"Street Name\">\r\n            </mat-form-field>\r\n        </div>\r\n        <div class=\"row\">\r\n            <mat-form-field class=\"reg-full-width\">\r\n                <input matInput type=\"text\" [(ngModel)]=\"state\" name=\"state\" class=\"form-control\" id=\"state\" placeholder=\"State Name\">\r\n            </mat-form-field>\r\n        </div>\r\n        <div class=\"row\">\r\n            <mat-form-field class=\"reg-full-width\">\r\n                <input matInput type=\"text\" [(ngModel)]=\"district\" name=\"district\" class=\"form-control\" id=\"district\" placeholder=\"District Name\">\r\n            </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"row\">\r\n            <mat-form-field class=\"reg-full-width\">\r\n                <input matInput type=\"text\" [(ngModel)]=\"taluk\" name=\"taluk\" class=\"form-control\" id=\"taluk\" placeholder=\"Taluk Name\">\r\n            </mat-form-field>\r\n        </div>\r\n        <div class=\"row\">\r\n            <mat-form-field class=\"reg-full-width\">\r\n                <input matInput type=\"text\" [(ngModel)]=\"village\" name=\"village\" class=\"form-control\" id=\"village\" placeholder=\"Village Name\">\r\n            </mat-form-field>\r\n        </div>\r\n        <div class=\"row\">\r\n            <mat-form-field class=\"reg-full-width\">\r\n                <input matInput type=\"text\" [(ngModel)]=\"pincode\" name=\"pincode\" class=\"form-control\" id=\"pincode\" placeholder=\"Pin Code\">\r\n            </mat-form-field>\r\n        </div>\r\n        <div class=\"row\">\r\n            <mat-form-field class=\"reg-full-width\">\r\n                <input matInput type=\"text\" [(ngModel)]=\"username\" name=\"username\" class=\"form-control\" id=\"username\" placeholder=\"Enter UserName\">\r\n            </mat-form-field>\r\n        </div>\r\n        \r\n        <div class=\"row\">\r\n            <mat-form-field class=\"reg-full-width\">\r\n                <input matInput type=\"text\" [(ngModel)]=\"email\" name=\"email\" class=\"form-control\" id=\"email\" placeholder=\"Enter E-Mail\">\r\n            </mat-form-field>\r\n        </div>\r\n        <div class=\"row\">\r\n            <mat-form-field class=\"reg-full-width\">\r\n                <input matInput type=\"text\" [(ngModel)]=\"mobile\" name=\"mobile\" class=\"form-control\" id=\"mobile\" placeholder=\"Enter Mobile Number\">\r\n            </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"row\">\r\n            <mat-form-field class=\"reg-full-width\">\r\n                <input matInput type=\"password\" [(ngModel)]=\"password\" name=\"password\" class=\"form-control\" id=\"password\" placeholder=\"Enter Password\">\r\n            </mat-form-field>\r\n        </div>\r\n      </mat-card-content>\r\n      <mat-card-actions>\r\n          <button mat-raised-button color=\"accent\" type=\"submit\">Register</button>\r\n      </mat-card-actions>\r\n    </form>\r\n    </mat-card>\r\n    </div>\r\n"

/***/ }),

/***/ "./src/app/components/reg-form/reg-form.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/components/reg-form/reg-form.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".full-width {\n  width: 100%; }\n\n.shipping-card {\n  min-width: 120px;\n  margin: 20px auto; }\n\n.mat-radio-button {\n  display: block;\n  margin: 5px 0; }\n\n.row {\n  display: flex;\n  flex-direction: row; }\n\n.column {\n  display: flex;\n  flex-direction: column; }\n\n.col {\n  flex: 1;\n  margin-right: 20px; }\n\n.col:last-child {\n  margin-right: 0; }\n\n.reg-full-width {\n  width: 100%; }\n\n.form-card {\n  min-width: 150px;\n  max-width: 700px;\n  width: 100%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9yZWctZm9ybS9EOlxcU291cmNlX0NvZGVcXEhhY0thVGhvblxcZmFybS1pcnJpZ2F0ZVxcZmFybUlycmlnYXRlQXBwXFxhbmd1bGFyLXNyYy9zcmNcXGFwcFxcY29tcG9uZW50c1xccmVnLWZvcm1cXHJlZy1mb3JtLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBVyxFQUNaOztBQUVEO0VBQ0UsaUJBQWdCO0VBQ2hCLGtCQUFpQixFQUNsQjs7QUFFRDtFQUNFLGVBQWM7RUFDZCxjQUFhLEVBQ2Q7O0FBRUQ7RUFDRSxjQUFhO0VBQ2Isb0JBQW1CLEVBQ3BCOztBQUNEO0VBQ0UsY0FBYTtFQUNiLHVCQUFzQixFQUN2Qjs7QUFFRDtFQUNFLFFBQU87RUFDUCxtQkFBa0IsRUFDbkI7O0FBRUQ7RUFDRSxnQkFBZSxFQUNoQjs7QUFFRDtFQUNFLFlBQVcsRUFDWjs7QUFDRDtFQUNFLGlCQUFnQjtFQUNoQixpQkFBZ0I7RUFDaEIsWUFBVyxFQUNaIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9yZWctZm9ybS9yZWctZm9ybS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mdWxsLXdpZHRoIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLnNoaXBwaW5nLWNhcmQge1xyXG4gIG1pbi13aWR0aDogMTIwcHg7XHJcbiAgbWFyZ2luOiAyMHB4IGF1dG87XHJcbn1cclxuXHJcbi5tYXQtcmFkaW8tYnV0dG9uIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBtYXJnaW46IDVweCAwO1xyXG59XHJcblxyXG4ucm93IHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbn1cclxuLmNvbHVtbiB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcblxyXG4uY29sIHtcclxuICBmbGV4OiAxO1xyXG4gIG1hcmdpbi1yaWdodDogMjBweDtcclxufVxyXG5cclxuLmNvbDpsYXN0LWNoaWxkIHtcclxuICBtYXJnaW4tcmlnaHQ6IDA7XHJcbn1cclxuXHJcbi5yZWctZnVsbC13aWR0aCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuLmZvcm0tY2FyZCB7XHJcbiAgbWluLXdpZHRoOiAxNTBweDtcclxuICBtYXgtd2lkdGg6IDcwMHB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/reg-form/reg-form.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/reg-form/reg-form.component.ts ***!
  \***********************************************************/
/*! exports provided: RegFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegFormComponent", function() { return RegFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _services_validate_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../services/validate.service */ "./src/app/services/validate.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng_flash_messages__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng-flash-messages */ "./node_modules/ng-flash-messages/ng-flash-messages.umd.js");
/* harmony import */ var ng_flash_messages__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(ng_flash_messages__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var RegFormComponent = /** @class */ (function () {
    function RegFormComponent(validateService, flashMessage, router, http) {
        this.validateService = validateService;
        this.flashMessage = flashMessage;
        this.router = router;
        this.http = http;
        this.accountType = "Farmer";
    }
    RegFormComponent.prototype.onRegisterSubmit = function () {
        var _this = this;
        var user = {
            name: this.name,
            doorNo: this.doorNo,
            streetName: this.streetName,
            state: this.state,
            district: this.district,
            taluk: this.taluk,
            village: this.village,
            pincode: this.pincode,
            email: this.email,
            mobile: this.mobile,
            username: this.username,
            password: this.password,
            accountType: this.accountType
        };
        console.log(user);
        if (!this.validateService.validateRegister(user)) {
            this.flashMessage.showFlashMessage({
                messages: ['Please Fill all fields'],
                dismissible: true, timeout: 3000, type: 'danger'
            });
            return false;
        }
        if (!this.validateService.validateEmail(user.email)) {
            this.flashMessage.showFlashMessage({
                messages: ['Please Use Valid Email'],
                dismissible: true, timeout: 3000, type: 'danger'
            });
            return false;
        }
        var req = this.http.post('http://localhost:3000/users/register', user).subscribe(function (res) {
            console.log(res);
            _this.flashMessage.showFlashMessage({
                messages: ['You Are Now Registered and can Login'],
                dismissible: true, timeout: 3000, type: 'success'
            });
            _this.router.navigate(['/login']);
        }, function (err) {
            console.log('Error Occured');
            _this.flashMessage.showFlashMessage({
                messages: ['Something Went Wrong'],
                dismissible: true, timeout: 3000, type: 'danger'
            });
            _this.router.navigate(['/']);
        });
        console.log(req);
    };
    RegFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-reg-form',
            template: __webpack_require__(/*! ./reg-form.component.html */ "./src/app/components/reg-form/reg-form.component.html"),
            styles: [__webpack_require__(/*! ./reg-form.component.scss */ "./src/app/components/reg-form/reg-form.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_validate_service__WEBPACK_IMPORTED_MODULE_2__["ValidateService"],
            ng_flash_messages__WEBPACK_IMPORTED_MODULE_4__["NgFlashMessageService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], RegFormComponent);
    return RegFormComponent;
}());



/***/ }),

/***/ "./src/app/guard/auth.guard.ts":
/*!*************************************!*\
  !*** ./src/app/guard/auth.guard.ts ***!
  \*************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");




var AuthGuard = /** @class */ (function () {
    function AuthGuard(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        if (this.auth.isLoggedIn()) {
            return true;
        }
        else {
            this.router.navigate(["/login"]);
            return false;
        }
    };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");




var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json' })
};
var AuthService = /** @class */ (function () {
    function AuthService(http) {
        this.http = http;
    }
    AuthService.prototype.handleError = function (error) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        }
        else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error("Backend returned code " + error.status + ", " +
                ("body was: " + error.error));
        }
        // return an observable with a user-facing error message
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["throwError"])('Something bad happened; please try again later.');
    };
    AuthService.prototype.registerUser = function (user) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'application/json');
        return this.http.post('http://localhost:3000/users/register', user);
    };
    AuthService.prototype.storeUserData = function (token, user) {
        localStorage.setItem('id_token', token);
        localStorage.setItem('user', JSON.stringify(user));
        this.authToken = token;
        this.user = user;
    };
    AuthService.prototype.logout = function () {
        this.authToken = null;
        this.user = null;
        localStorage.clear();
    };
    AuthService.prototype.sendToken = function (token) {
        localStorage.setItem("LoggedInUser", token);
    };
    AuthService.prototype.getToken = function () {
        return localStorage.getItem("user");
    };
    AuthService.prototype.isLoggedIn = function () {
        return this.getToken() !== null;
    };
    AuthService.prototype.accountType = function () {
        if (this.user.accountType == "farmer") {
            return true;
        }
    };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/form.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/form.service.ts ***!
  \******************************************/
/*! exports provided: FormService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormService", function() { return FormService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FormService = /** @class */ (function () {
    function FormService() {
    }
    FormService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FormService);
    return FormService;
}());



/***/ }),

/***/ "./src/app/services/validate.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/validate.service.ts ***!
  \**********************************************/
/*! exports provided: ValidateService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidateService", function() { return ValidateService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ValidateService = /** @class */ (function () {
    function ValidateService() {
    }
    ValidateService.prototype.validateRegister = function (user) {
        // tslint:disable-next-line:triple-equals
        if (user.name == undefined || user.username == undefined || user.email == undefined || user.password == undefined) {
            return false;
        }
        else {
            return true;
        }
    };
    ValidateService.prototype.validateFarm = function (farm) {
        // tslint:disable-next-line:triple-equals
        if (farm.cropName == undefined || farm.cultiArea == undefined || farm.fertilizer == undefined || farm.insecti == undefined || farm.yeild == undefined || farm.retailer == undefined || farm.season == undefined) {
            return false;
        }
        else {
            return true;
        }
    };
    ValidateService.prototype.validateLogin = function (user) {
        // tslint:disable-next-line:triple-equals
        if (user.username == undefined || user.password == undefined) {
            return false;
        }
        else {
            return true;
        }
    };
    ValidateService.prototype.validateEmail = function (email) {
        // tslint:disable-next-line:max-line-length
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };
    ValidateService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ValidateService);
    return ValidateService;
}());



/***/ }),

/***/ "./src/app/theme/material.module.ts":
/*!******************************************!*\
  !*** ./src/app/theme/material.module.ts ***!
  \******************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_extensions_jumbotron__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular-material-extensions/jumbotron */ "./node_modules/@angular-material-extensions/jumbotron/esm5/jumbotron.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");




var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [],
            imports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCheckboxModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatExpansionModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatToolbarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatChipsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSelectModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatRadioModule"], _angular_material_extensions_jumbotron__WEBPACK_IMPORTED_MODULE_2__["MatJumbotronModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatFormFieldModule"]
            ],
            exports: [_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCheckboxModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatExpansionModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatToolbarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatChipsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSelectModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatRadioModule"], _angular_material_extensions_jumbotron__WEBPACK_IMPORTED_MODULE_2__["MatJumbotronModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatFormFieldModule"]
            ]
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\Source_Code\HacKaThon\farm-irrigate\farmIrrigateApp\angular-src\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map