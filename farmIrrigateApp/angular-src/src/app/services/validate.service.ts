import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidateService {

  constructor() { }

  validateRegister(user) {
    // tslint:disable-next-line:triple-equals
    if (user.name == undefined || user.username == undefined || user.email == undefined || user.password == undefined) {
      return false;
    } else {
      return true;
    }
  }
  validateFarm(farm) {
    // tslint:disable-next-line:triple-equals
    if (farm.cropName == undefined || farm.cultiArea == undefined || farm.fertilizer == undefined || farm.insecti == undefined || farm.yeild == undefined || farm.retailer == undefined || farm.season == undefined) {
      return false;
    } else {
      return true;
    }
  }
  validateLogin(user) {
    // tslint:disable-next-line:triple-equals
    if (user.username == undefined || user.password == undefined) {
      return false;
    } else {
      return true;
    }
  }
  validateEmail(email) {
    // tslint:disable-next-line:max-line-length
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
}
