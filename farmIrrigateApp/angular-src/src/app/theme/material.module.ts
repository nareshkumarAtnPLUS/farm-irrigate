import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatJumbotronModule } from '@angular-material-extensions/jumbotron';
import * as Material from '@angular/material';

@NgModule({
  declarations: [],
  imports: [
    Material.MatButtonModule, Material.MatCheckboxModule, Material.MatMenuModule,
        Material.MatExpansionModule, Material.MatToolbarModule, Material.MatSidenavModule,
        Material.MatIconModule, Material.MatListModule, Material.MatGridListModule,
        Material.MatCardModule, Material.MatChipsModule,  Material.MatInputModule,
       Material.MatSelectModule, Material.MatRadioModule, MatJumbotronModule,Material.MatFormFieldModule
  ],
  exports: [ Material.MatButtonModule, Material.MatCheckboxModule, Material.MatMenuModule,
    Material.MatExpansionModule, Material.MatToolbarModule, Material.MatSidenavModule,
    Material.MatIconModule, Material.MatListModule, Material.MatGridListModule,
    Material.MatCardModule, Material.MatChipsModule, Material.MatInputModule,
   Material.MatSelectModule, Material.MatRadioModule, MatJumbotronModule,Material.MatFormFieldModule
  ]
})
export class MaterialModule { }
