import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ValidateService } from './../../services/validate.service';
import { Component } from '@angular/core';
import { NgFlashMessageService } from 'ng-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reg-form',
  templateUrl: './reg-form.component.html',
  styleUrls: ['./reg-form.component.scss'],
})
export class RegFormComponent {

  name: String;
  doorNo:String;
  streetName:String;
  state:String;
  district:String;
  taluk:String;
  village:String;
  pincode:String;
  username: String;
  email: String;
  mobile:String;
  fieldNo:String;
  fieldSize:String;
  accountType:String = "Farmer";
  password: String;
  ROOT_URL: 'http://localhost:3000';
  newPost: Observable<any>;

  constructor(private validateService: ValidateService,
    private flashMessage: NgFlashMessageService,
    private router: Router,
    private http: HttpClient
    ) {}

  onRegisterSubmit() {
    const user = {
      name: this.name,
      doorNo:this.doorNo,
      streetName:this.streetName,
      state:this.state,
      district:this.district,
      taluk:this.taluk,
      village:this.village,
      pincode:this.pincode,
      email: this.email,
      mobile:this.mobile,
      fieldNo:this.fieldNo,
      fieldSize:this.fieldSize,
      username: this.username,
      password: this.password,
      accountType:this.accountType
    };
    console.log(user);
    if (!this.validateService.validateRegister(user)) {
      this.flashMessage.showFlashMessage({
        messages: ['Please Fill all fields'],
         dismissible: true, timeout: 3000, type: 'danger'
        });
      return false;
    }
    if (!this.validateService.validateEmail(user.email)) {
      this.flashMessage.showFlashMessage({
        messages: ['Please Use Valid Email'],
        dismissible: true, timeout: 3000, type: 'danger'
        });
      return false;
    }

    const req = this.http.post('http://localhost:3000/users/register', user).subscribe(
      res => {
        console.log(res);
        this.flashMessage.showFlashMessage({
          messages: ['You Are Now Registered and can Login'],
          dismissible: true, timeout: 3000, type: 'success'
          });
        this.router.navigate(['/login']);
      },
      err => {
        console.log('Error Occured');
        this.flashMessage.showFlashMessage({
          messages: ['Something Went Wrong'],
          dismissible: true, timeout: 3000, type: 'danger'
          });
        this.router.navigate(['/']);
      });
      console.log(req);
  }
}
