import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { NgFlashMessageService } from 'ng-flash-messages';
import { HttpClient, HttpHeaders } from '@angular/common/http';


interface Data {
  farmData: Object;
}@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router,
    private flashMessage: NgFlashMessageService,
    private http: HttpClient) { }
    datum:any;
  getData() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': this.authService.authToken
      })
    };
    this.loadToken();
    console.log(this.authService.authToken);
    return this.http.get<Data>('http://localhost:3000/users/dashboard', httpOptions);
  }

  ngOnInit() {
    this.getData().subscribe(profile => {
      this.datum = profile.farmData;
      if (!(this.datum == null)) {
        console.log(this.datum);
      } else {
        console.log('UnAuthorized Access!');
      }
    },
    (err) => {
      return false;
    });
  }
  loadToken() {
    const token = localStorage.getItem('id_token');
    this.authService.authToken = token;
  }

}
