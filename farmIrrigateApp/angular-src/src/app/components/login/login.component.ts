import { AuthService } from './../../services/auth.service';
import { NgFlashMessageService } from 'ng-flash-messages';
import { ValidateService } from './../../services/validate.service';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

export interface Res {
  success: boolean;
  user: Object;
  token:  Object;
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent {
  username: String;
  password: String;


  constructor(
    private router: Router,
    private flashMessage: NgFlashMessageService,
    private http: HttpClient,
    private validateService: ValidateService,
    private authService: AuthService
  ) { }

  onLoginSubmit() {
    const user = {
      username: this.username,
      password: this.password
    };
    if (!this.validateService.validateLogin(user)) {
      this.flashMessage.showFlashMessage({
        messages: ['Please Fill all fields'],
         dismissible: true, timeout: 3000, type: 'danger'
        });
      return false;
    }
    const req = this.http.post<Res>('http://localhost:3000/users/authenticate', user).subscribe(
      res => {
        console.log(res.success);
        if (res.success) {
          this.authService.storeUserData(res.token, res.user);
          this.flashMessage.showFlashMessage({
          messages: ['You Are Now Logged In'],
          dismissible: true, timeout: 3000, type: 'success'
          });
        this.router.navigate(['/profile']);
        console.log(res.token, res.user);
        } else {
          this.flashMessage.showFlashMessage({
            messages: ['Check Login Credentials,User Name or password mismatch'],
          dismissible: true, timeout: 5000, type: 'danger'
          });
          this.router.navigate(['/login']);
        }
      },
      err => {
        console.log('Error Occured');
        this.flashMessage.showFlashMessage({
          messages: ['Something Went Wrong'],
          dismissible: true, timeout: 3000, type: 'danger'
          });
        this.router.navigate(['/login']);
      });
      console.log(req);
  }
}
