import { AuthService } from './../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NgFlashMessageService } from 'ng-flash-messages';
import { ValidateService } from 'src/app/services/validate.service';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss'],
})
export class MainNavComponent {
  //opener:false;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private flashMessage: NgFlashMessageService,
    private http: HttpClient,
    private validateService: ValidateService,
    private authService: AuthService
    ) { }
    
  onLogoutClick() {
    this.authService.logout();
    this.flashMessage.showFlashMessage({
      messages: ['You Are Now Logged Out'],
      dismissible: true, timeout: 3000, type: 'success'
      });
      this.router.navigate(['/login']);
      return false;
  }

}
