import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ValidateService } from '../../services/validate.service';
import { Component } from '@angular/core';
import { NgFlashMessageService } from 'ng-flash-messages';
import { Router } from '@angular/router';
@Component({
  selector: 'app-farm-details',
  templateUrl: './farm-details.component.html',
  styleUrls: ['./farm-details.component.scss']
})
export class FarmDetailsComponent {

  cropName: String;
  cultiArea:String;
  fertilizer:String;
  insecti:String;
  yeild:String;
  retailer:String;
  season:String;
  farmerName:String;
  farmUserName: String;
  farmerId:String;
  ROOT_URL: 'http://localhost:3000';
  newPost: Observable<any>;
  
  constructor(private validateService: ValidateService,
    private flashMessage: NgFlashMessageService,
    private router: Router,
    private http: HttpClient
    ) {}

  onFarmSubmit() {
    const farmData = {
      cropName: this.cropName,
      cultiArea:this.cultiArea,
      fertilizer:this.fertilizer,
      insecti:this.insecti,
      yeild:this.yeild,
      retailer:this.retailer,
      season:this.season,
      farmUserName:this.farmUserName,
      farmerName:this.farmerName,
      //farmerId: this.farmerId,
    };
    console.log(farmData);
    if (!this.validateService.validateFarm(farmData)){
      this.flashMessage.showFlashMessage({
        messages: ['Please Fill all fields'],
         dismissible: true, timeout: 3000, type: 'danger'
        });
      return false;
    }
    const req = this.http.post('http://localhost:3000/users/farmDetails', farmData).subscribe(
      res => {
        console.log(res);
        this.flashMessage.showFlashMessage({
          messages: ['Farm Data Successfully Uploaded to Portal'],
          dismissible: true, timeout: 3000, type: 'success'
          });
        this.router.navigate(['/farmDetails']);
      },
      err => {
        console.log('Error Occured');
        this.flashMessage.showFlashMessage({
          messages: ['Something Went Wrong'],
          dismissible: true, timeout: 3000, type: 'danger'
          });
        this.router.navigate(['/']);
      });
      console.log(req);
  } 
}
