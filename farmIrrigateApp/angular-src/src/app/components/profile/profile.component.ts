import { Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgFlashMessageService } from 'ng-flash-messages';
interface Profile {
  user: Object;
}
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  accountType:boolean;
  constructor(
    private authService: AuthService,
    private router: Router,
    private flashMessage: NgFlashMessageService,
    private http: HttpClient) { }
    user: Object;
  getProfile() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': this.authService.authToken
      })
    };
    this.loadToken();
    console.log('user Data',this.authService.authToken);
    return this.http.get<Profile>('http://localhost:3000/users/profile', httpOptions);
  }
  ngOnInit() {
    this.getProfile().subscribe(profile => {
      this.user = profile.user;
      if (!(this.user == null)) {
      } else {
        console.log('UnAuthorized Access!');
      }
    },
    (err) => {
      return false;
    });
  }
  loadToken() {
    const token = localStorage.getItem('id_token');
    this.authService.authToken = token;
  }
}
